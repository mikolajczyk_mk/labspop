module Lab1 where
import Data.Char

g:: Int -> Int
g = (+1)
--------------------------------------------------------------------
---------------      Task 1          --------------------------------
--------------------------------------------------------------------
middle :: [a] -> a
middle [] =  error "Empty list"
middle (x) | mod (length x) 2 == 0 = error "Even size of list"
           | otherwise             = x !! ((div (length x) 2) )

--------------------------------------------------------------------
----------------     Task 2          --------------------------------
--------------------------------------------------------------------

removeDups :: Eq a => [a] -> [a]
removeDups [] = []
removeDups [x] = [x]
removeDups (x:y:ys) = if x == y then removeDups(y:ys)
                      else [x] ++ removeDups(y:ys)


--------------------------------------------------------------------
----------------     Task 3          --------------------------------
--------------------------------------------------------------------

removeAllDups :: Eq a => [a] -> [a]
removeAllDups [] =  []
removeAllDups [x] = [x]
removeAllDups (l) = seen l []
                     where seen [] list = list
                           seen (x:xs) list = if elem x list then seen xs list
                                              else seen xs (list ++ [x])

--------------------------------------------------------------------
----------------     Task 4          --------------------------------
--------------------------------------------------------------------
adjpairs :: [a] -> [(a,a)]
adjpairs [] = []
adjpairs [x] = []
adjpairs (x:y:ys) = [(x,y)] ++ adjpairs(y:ys)

--------------------------------------------------------------------
----------------     Task 5          --------------------------------
--------------------------------------------------------------------
string2int :: String -> Int
string2int (x:xs) | isDigit x == False = negate (string2int xs)
                  | otherwise          = convert x xs (length (xs))
                                          where convert x [] acc = (10^acc) * digitToInt x
                                                convert x [y] acc = (10^acc) * digitToInt x + digitToInt y
                                                convert x (y:ys) acc =  (10^acc) * digitToInt x + convert y ys (acc-1)

--------------------------------------------------------------------
----------------     Task 6          --------------------------------
--------------------------------------------------------------------
insertAt :: [a] -> a -> Int -> [a]
insertAt x a 0 = [a]++x
insertAt [] _ _ = error "Wrong Index"
insertAt (x:xs) a index = [x] ++ (insertAt xs a (index-1))

--------------------------------------------------------------------
----------------     Task 7          --------------------------------
--------------------------------------------------------------------
codeCezar :: String -> Int -> String
codeCezar [] _ = []
codeCezar str 0 = str
codeCezar (x:xs) key | key < 0 = error "Wrong key"
                     | (isUpper x && (ord x + key) <= 90) || (isLower x && (ord x + key) <= 122) = [chr((ord x) + key)] ++ codeCezar xs key
                     | isUpper x && (ord x + key) > 90                                           = [chr(mod (ord x + key) 90 + 64)] ++ codeCezar xs key
                     | isLower x && (ord x + key) > 122                                          = [chr(mod (ord x + key) 122 + 96)] ++ codeCezar xs key
                     | otherwise                                                                 = error "Character unsupported"

decodeCezar :: String -> Int -> String
decodeCezar [] _ = []
decodeCezar str 0 = str
decodeCezar (x:xs) key | key < 0 = error "Wrong key"
                     | (isUpper x && (ord x - key) >= 65) || (isLower x && (ord x - key) >= 97) = [chr((ord x) - key)] ++ decodeCezar xs key
                     | isUpper x && (ord x - key) < 65                                          = [chr(90 - mod 65 (ord x - key) + 1)] ++ decodeCezar xs key
                     | isLower x && (ord x - key) < 97                                          = [chr(122 - mod 97 (ord x - key) + 1)] ++ decodeCezar xs key
                     | otherwise                                                                = error "Character unsupported"
