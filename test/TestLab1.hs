module Main where
import Test.HUnit as TTest
import Test.Framework
import Test.Framework.Providers.HUnit
import Data.Monoid
import Control.Monad
import Lab1

testTask1 :: TTest.Test
testTask1 = test ["Task 1" ~: "1. middle Haskell"       ~: 'k' ~=? (middle "Haskell"),
                 "Task 1" ~: "2. middle Ala"           ~: 'l' ~=? (middle "Ala"),
                 "Task 1" ~: "3. middle Ala ma Kota"   ~: 'a' ~=? (middle "Ala ma Kota"),
                 "Task 1" ~: "4. middle Gruszka"       ~: 's' ~=? (middle "Gruszka"),
                 "Task 1" ~: "5. middle laboratorium1" ~: 't' ~=? (middle "laboratorium1")]

testTask2 :: TTest.Test
testTask2 = test ["Task 2" ~: "1. removeDups [9, 3, 3, 3, 4, 5, 5, 3, 5]" ~: [9,3,4,5,3,5]            ~=? (removeDups  [9, 3, 3, 3, 4, 5, 5, 3, 5] ),
                 "Task 2" ~: "2. removeDups [3, 3, 3, 4, 4, 5, 5, 3, 5]" ~: [3,4,5,3,5]              ~=? (removeDups  [3, 3, 3, 4, 4, 5, 5, 3, 5] ),
                 "Task 2" ~: "3. removeDups [1, 1, 3, 3, 4, 4, 4, 5, 4]" ~: [1,3,4,5,4]              ~=? (removeDups  [1, 1, 3, 3, 4, 4, 4, 5, 4] ),
                 "Task 2" ~: "4. removeDups [1, 2, 3, 4, 5, 6, 6, 1, 2]" ~: [1, 2, 3, 4, 5, 6, 1, 2] ~=? (removeDups  [1, 2, 3, 4, 5, 6, 6, 1, 2] ),
                 "Task 2" ~: "5. removeDups [9, 9, 3, 3, 4, 5, 5, 3, 5]" ~: [9,3,4,5,3,5]            ~=? (removeDups  [9, 9, 3, 3, 4, 5, 5, 3, 5] )]

testTask3 :: TTest.Test
testTask3 = test ["Task 3" ~: "1. removeAllDups [9, 3, 3, 3, 4, 5, 5, 3, 5]" ~: [9,3,4,5]          ~=? (removeAllDups  [9, 3, 3, 3, 4, 5, 5, 3, 5] ),
                 "Task 3" ~: "2. removeAllDups [3, 3, 3, 4, 4, 5, 5, 3, 5]" ~: [3,4,5]            ~=? (removeAllDups  [3, 3, 3, 4, 4, 5, 5, 3, 5] ),
                 "Task 3" ~: "3. removeAllDups [1, 1, 3, 3, 4, 4, 4, 5, 4]" ~: [1,3,4,5]          ~=? (removeAllDups  [1, 1, 3, 3, 4, 4, 4, 5, 4] ),
                 "Task 3" ~: "4. removeAllDups [1, 2, 3, 4, 5, 6, 6, 1, 2]" ~: [1, 2, 3, 4, 5, 6] ~=? (removeAllDups  [1, 2, 3, 4, 5, 6, 6, 1, 2] ),
                 "Task 3" ~: "5. removeAllDups [9, 9, 3, 3, 4, 5, 5, 3, 5]" ~: [9,3,4,5]          ~=? (removeAllDups  [9, 9, 3, 3, 4, 5, 5, 3, 5] )]

testTask4 :: TTest.Test
testTask4 = test ["Task 4" ~: "1. adjpairs [1, 2, 3, 4] "       ~: [(1,2), (2,3), (3,4)]              ~=? (adjpairs [1, 2, 3, 4] ),
                 "Task 4" ~: "2. adjpairs [1, 1, 2, 3, 4, 5] " ~: [(1,1),(1,2), (2,3), (3,4), (4,5)] ~=? (adjpairs [1, 1, 2, 3, 4, 5] ),
                 "Task 4" ~: "3. adjpairs [1, 1, 3, 3, 5, 4] " ~: [(1,1),(1,3), (3,3), (3,5), (5,4)] ~=? (adjpairs [1, 1, 3, 3, 5, 4] ),
                 "Task 4" ~: "4. adjpairs [9, 8, 7, 6, 5] "    ~: [(9,8),(8,7), (7,6), (6,5)]        ~=? (adjpairs [9,8,7,6,5] ),
                 "Task 4" ~: "5. adjpairs [1] "                ~: []                                 ~=? (adjpairs [1] )]

testTask5 :: TTest.Test
testTask5 = test ["Task 5" ~: "1. string2int 356"     ~:  356    ~=? (string2int "356" ),
                 "Task 5" ~: "2. string2int -23"     ~: -23     ~=? (string2int "-23" ),
                 "Task 5" ~: "3. string2int -141"    ~: -141    ~=? (string2int "-141" ),
                 "Task 5" ~: "4. string2int 789456"  ~:  789456 ~=? (string2int "789456" ),
                 "Task 5" ~: "5. string2int -789456" ~: -789456 ~=? (string2int "-789456" )]

testTask6 :: TTest.Test
testTask6 = test ["Task 6" ~: "1. insertAt askell H 0 "                  ~:  "Haskell"                  ~=? (insertAt "askell" 'H' 0  ),
                 "Task 6" ~: "2. insertAt [(1,2), (3,4)] (1,1) 2 "      ~: [(1,2), (3,4), (1,1)]       ~=? (insertAt [(1,2), (3,4)] (1,1) 2  ),
                 "Task 6" ~: "3. insertAt askell H 1"                   ~: "aHskell"                   ~=? (insertAt "askell" 'H' 1 ),
                 "Task 6" ~: "4. insertAt [1,2,3,4,5] 6 5"              ~:  [1,2,3,4,5,6]              ~=? (insertAt [1,2,3,4,5] 6 5 ),
                 "Task 6" ~: "5. insertAt [(1,2,1), (2,3,4)] (0,0,0) 1" ~: [(1,2,1), (0,0,0), (2,3,4)] ~=? (insertAt [(1,2,1), (2,3,4)] (0,0,0) 1 )]

testTask7:: TTest.Test
testTask7 = test ["Task 7" ~: "1. CodeCezar - UpperCase alphabet, step 5"   ~: "FGHIJKLMNOPQRSTUVWXYZABCDE" ~=? (codeCezar "ABCDEFGHIJKLMNOPQRSTUVWXYZ" 5),
                 "Task 7" ~: "1. CodeCezar - LowerCase alphabet, step 5"   ~: "fghijklmnopqrstuvwxyzabcde" ~=? (codeCezar "abcdefghijklmnopqrstuvwxyz" 5),
                 "Task 7" ~: "1. DecodeCezar - LowerCase alphabet, step 5" ~: "abcdefghijklmnopqrstuvwxyz" ~=? (decodeCezar "fghijklmnopqrstuvwxyzabcde" 5),
                 "Task 7" ~: "1. DecodeCezar - LowerCase alphabet, step 5" ~: "ABCDEFGHIJKLMNOPQRSTUVWXYZ" ~=? (decodeCezar "FGHIJKLMNOPQRSTUVWXYZABCDE" 5)]

main :: IO Counts
main = do _ <- runTestTT testTask1
          runTestTT testTask2
          runTestTT testTask3
          runTestTT testTask4
          runTestTT testTask5
          runTestTT testTask6
          runTestTT testTask7